# My Beer Half ❤🍺

Here there's some documentation for you all, folks!

## User's guide 📝

This app has been created to find the best beer to be related to your meal. Enjoy it!

### Let's start 📱

It is so easy, just follow these two steps!

1.  Type your your meal.<br>
<img src="/images/start.png"  width="150" height="300">
<br>
2.  Get a list of the best beers ever tried before to come along with your meal!<br><br>
<img src="/images/beers.png"  width="150" height="300">

## Dev 🔧

### Beers' source 🍻
The data collected comes from [Punk API V2](https://punkapi.com/documentation/v2).

### Translations 🈂

Get last translations' changes [here](https://localise.biz/api/export/locale/en.xml?format=android&key=kW4puYXRmRLGoiqr5D9V-c-J_J4uMqJa).

❗ Pssst... If you dare, you can ask acces for the [translations project](https://localise.biz/mybeerhalf)...

### Test e-mail 📧

Use this fake [e-mail](https://www.mailinator.com/v3/index.jsp?zone=public&query=mybeerhalf#/#inboxpane) for any test or whatever you need related to the project.