package com.avanwalre.mybeerhalf.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.avanwalre.mybeerhalf.R;
import com.avanwalre.mybeerhalf.adapters.MatchersBeersAdapter;
import com.avanwalre.mybeerhalf.apis.ApiInvoker;
import com.avanwalre.mybeerhalf.database.AppDatabase;
import com.avanwalre.mybeerhalf.dto.Beer;
import com.avanwalre.mybeerhalf.entities.SearchHistory;
import com.avanwalre.mybeerhalf.enums.Extras;
import com.avanwalre.mybeerhalf.utils.SearchHistoryUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class MatchesActivity extends AppCompatActivity {

    // Constants
    private static final String HOST = "https://api.punkapi.com/v2/";

    private RecyclerView recyclerView;
    private MatchersBeersAdapter recyclerViewAdapter;
    private boolean isAbvIncOrder = true;
    private List<Beer> beerList;
    private Menu menu;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout layoutNoResults;
    private boolean isErrorOn = false;
    private String foodName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.matches_layout);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.matches_progressBar);
        layoutNoResults = findViewById(R.id.matches_layout_noresult);

        // Init
        initSwipeRefreshLayout();
        initBeerRecyclerView();

        // First checks if there's offline data
        loadData();
    }

    /**
     * Decides if data is needed to be collected from the Internet.
     */
    private void loadData() {
        final String TAG = this.getClass().getSimpleName() + "@loadData";

        foodName = Objects.requireNonNull(this.getIntent().getStringExtra(Extras.EXTRA_FOOD.key())).replace(" ", "_").toLowerCase();

        Log.i(TAG, "Init for food name '" + foodName + "'");

        SearchHistory searchHistory = SearchHistoryUtil.getSearchHistory(AppDatabase.getAppDatabase(this), foodName);

        if (searchHistory != null) {
            loadJsonData(searchHistory.getJsonData());
        } else {
            callApi();
        }
    }

    /**
     * Calls API for obtaining data.
     */
    private void callApi() {
        final String TAG = this.getClass().getSimpleName() + "@callApi";

        Log.i(TAG, "Getting online data for food name '" + foodName + "'");

        ApiInvoker apiInvoker = ApiInvoker.getInstance(this);
        final String path = "beers?food=" + foodName;
        String url = HOST + path;

        // Request
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.equals("[]")) {
                            saveToDatabase(response);
                        }
                        loadJsonData(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error while getting data: " + error.getMessage());
                        toggleProgressBar();
                        showError(true);
                    }
                }
        );

        // Invoke request
        apiInvoker.addToRequestQueue(stringRequest);
    }

    /**
     * Saves data to database for future requests.
     *
     * @param jsonData beer's json data
     */
    private void saveToDatabase(String jsonData) {
        SearchHistoryUtil.insertSearchHistory(AppDatabase.getAppDatabase(this), foodName, jsonData);
    }

    /**
     * Processes the obtained data.
     *
     * @param response data response
     */
    private void loadJsonData(String response) {
        final String TAG = this.getClass().getSimpleName() + "@loadJsonData";

        try {
            beerList = Arrays.asList(new ObjectMapper().readValue(response, Beer[].class));

            if (!beerList.isEmpty()) {
                orderByAbv();
                recyclerViewAdapter = new MatchersBeersAdapter(beerList);
                recyclerView.setAdapter(recyclerViewAdapter);
            } else {
                showError(false);
            }

            toggleProgressBar();
            swipeRefreshLayout.setRefreshing(false);
        } catch (IOException e) {
            Log.e(TAG, "Error while parsing data: " + e.getMessage());
        }
    }

    /**
     * Show message error in the middle of the screen.
     *
     * @param isConnectionFailure indicates if there's an error with the Internet connection
     */
    private void showError(boolean isConnectionFailure) {
        isErrorOn = true;

        if (isConnectionFailure) {
            TextView errorLabel = findViewById(R.id.matches_noresult);
            errorLabel.setText(R.string.matches_label_noconnection);
        }

        checkHidingActionButton();

        layoutNoResults.setVisibility(View.VISIBLE);
    }

    /**
     * Initialize beer recycler view.
     */
    private void initBeerRecyclerView() {
        recyclerView = findViewById(R.id.matches_rv_beers);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    /**
     * Initialize beer swipe refresh layout.
     */
    private void initSwipeRefreshLayout() {
        swipeRefreshLayout = findViewById(R.id.matches_swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                toggleProgressBar();
                loadData();
            }
        });
    }

    /**
     * Inc/dec order by ABV property.
     */
    private void orderByAbv() {
        // Ordering data set
        Collections.sort(beerList, new Comparator<Beer>() {
            @Override
            public int compare(Beer beer1, Beer beer2) {
                return isAbvIncOrder ? beer1.getAbv().compareTo(beer2.getAbv()) : beer2.getAbv().compareTo(beer1.getAbv());
            }
        });
    }

    /**
     * Changes drawable and text from ordering icon.
     */
    private void toggleOrderingIcon() {
        if (isAbvIncOrder) {
            menu.findItem(R.id.menu_top_matches_orderAbv).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_abv_dec)).setTitle(R.string.matches_actionbutton_order_inc);
        } else {
            menu.findItem(R.id.menu_top_matches_orderAbv).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_abv_inc)).setTitle(R.string.matches_actionbutton_order_dec);
        }
    }

    /**
     * Toggle progress bar visibility.
     */
    private void toggleProgressBar() {
        progressBar.setVisibility(progressBar.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    /**
     * Hides action buttton in case of error.
     */
    private void checkHidingActionButton() {
        if (menu != null && isErrorOn) {
            menu.findItem(R.id.menu_top_matches_orderAbv).setVisible(false);
        }
    }

    /**
     * Action bar buttons.
     *
     * @param item item selected
     * @return return if the item one is selected
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_top_matches_orderAbv:
                isAbvIncOrder = !isAbvIncOrder;
                orderByAbv();
                toggleOrderingIcon();
                recyclerViewAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Inflate top menu.
     *
     * @param menu menu to be inflated
     * @return menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        getMenuInflater().inflate(R.menu.menu_top_matches, menu);

        checkHidingActionButton();

        return super.onCreateOptionsMenu(menu);
    }

}
