package com.avanwalre.mybeerhalf.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.avanwalre.mybeerhalf.R;
import com.avanwalre.mybeerhalf.enums.Extras;

public class MainActivity extends Activity {
    
    private EditText fieldFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        Button buttonSearch = findViewById(R.id.main_button_search);
        fieldFood = findViewById(R.id.main_field_food);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String foodData = fieldFood.getText().toString();

                if (!foodData.isEmpty()) {
                    Intent intent = new Intent(getApplicationContext(), MatchesActivity.class);
                    intent.putExtra(Extras.EXTRA_FOOD.key(), fieldFood.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }

}
