
package com.avanwalre.mybeerhalf.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "temp"
})
public class Fermentation {

    @JsonProperty("temp")
    private Temp_ temp;

    @JsonProperty("temp")
    public Temp_ getTemp() {
        return temp;
    }

    @JsonProperty("temp")
    public void setTemp(Temp_ temp) {
        this.temp = temp;
    }

}
