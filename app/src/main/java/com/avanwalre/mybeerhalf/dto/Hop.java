
package com.avanwalre.mybeerhalf.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "amount",
    "add",
    "attribute"
})
public class Hop {

    @JsonProperty("name")
    private String name;
    @JsonProperty("amount")
    private Amount_ amount;
    @JsonProperty("add")
    private String add;
    @JsonProperty("attribute")
    private String attribute;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("amount")
    public Amount_ getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Amount_ amount) {
        this.amount = amount;
    }

    @JsonProperty("add")
    public String getAdd() {
        return add;
    }

    @JsonProperty("add")
    public void setAdd(String add) {
        this.add = add;
    }

    @JsonProperty("attribute")
    public String getAttribute() {
        return attribute;
    }

    @JsonProperty("attribute")
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

}
