package com.avanwalre.mybeerhalf.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.avanwalre.mybeerhalf.R;
import com.avanwalre.mybeerhalf.dto.Beer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MatchersBeersAdapter extends RecyclerView.Adapter<MatchersBeersAdapter.MyViewHolder> {

    private final List<Beer> beerList;

    public MatchersBeersAdapter(List<Beer> beerList) {
        this.beerList = beerList;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.matches_detail_layout, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        Beer beerItem = beerList.get(position);

        holder.tagline.setText(beerItem.getTagline());
        holder.beerName.setText(beerItem.getName());
        holder.description.setText(beerItem.getDescription());
        holder.abv.setText(String.format("%s%% ABV", beerItem.getAbv().toString()));
        Picasso.get().load(beerItem.getImageUrl()).placeholder(R.mipmap.logo_hi_res_512).into(holder.detailImg);
    }

    @Override
    public int getItemCount() {
        return beerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView abv;
        final TextView beerName;
        final TextView tagline;
        final TextView description;
        final ImageView detailImg;

        private MyViewHolder(View itemView) {
            super(itemView);

            abv = itemView.findViewById(R.id.matches_detail_label_abv);
            tagline = itemView.findViewById(R.id.matches_detail_label_tagline);
            beerName = itemView.findViewById(R.id.matches_detail_label_beername);
            description = itemView.findViewById(R.id.matches_detail_label_description);
            detailImg = itemView.findViewById(R.id.matches_detail_image);
        }
    }
}