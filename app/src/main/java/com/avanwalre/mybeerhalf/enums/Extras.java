package com.avanwalre.mybeerhalf.enums;

public enum Extras {
    EXTRA_FOOD("EXTRA_FOOD");

    private final String key;

    Extras (String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
