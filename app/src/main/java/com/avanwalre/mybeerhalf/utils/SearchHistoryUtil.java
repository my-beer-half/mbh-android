package com.avanwalre.mybeerhalf.utils;

import android.util.Log;

import com.avanwalre.mybeerhalf.database.AppDatabase;
import com.avanwalre.mybeerhalf.entities.SearchHistory;

public class SearchHistoryUtil {

    private SearchHistoryUtil() {
        // Utility class
    }

    public static SearchHistory getSearchHistory(AppDatabase database, String foodName) {
        final String TAG = SearchHistoryUtil.class.getSimpleName() + "@getSearchHistory";
        Log.i(TAG, "Getting search history data for food '" + foodName + "'");
        return database.searchHistoryDao().findByFoodName(foodName);
    }

    public static void insertSearchHistory(AppDatabase database, String foodName, String jsonData) {
        final String TAG = SearchHistoryUtil.class.getSimpleName() + "@insertSearchHistory";
        Log.i(TAG, "Inserting new search history data for food '" + foodName + "'");
        database.searchHistoryDao().insert(new SearchHistory().foodName(foodName).jsonData(jsonData));
    }

}
