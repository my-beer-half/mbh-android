package com.avanwalre.mybeerhalf.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "search_history")
public class SearchHistory {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "food_name")
    private String foodName;

    @ColumnInfo(name = "json_data")
    private String jsonData;

    public int getId() {
        return id;
    }

    public SearchHistory id(int id) {
        this.id = id;
        return this;
    }

    public String getFoodName() {
        return foodName;
    }

    public SearchHistory foodName(String foodName) {
        this.foodName = foodName;
        return this;
    }

    public String getJsonData() {
        return jsonData;
    }

    public SearchHistory jsonData(String jsonData) {
        this.jsonData = jsonData;
        return this;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }


}
