package com.avanwalre.mybeerhalf.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.avanwalre.mybeerhalf.dao.SearchHistoryDao;
import com.avanwalre.mybeerhalf.entities.SearchHistory;

@Database(entities = {SearchHistory.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    private static final String DATABASE_NAME = "mybeerhalf_db";

    public abstract SearchHistoryDao searchHistoryDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
        }

        return INSTANCE;
    }

}
