package com.avanwalre.mybeerhalf.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.avanwalre.mybeerhalf.entities.SearchHistory;

@Dao
public interface SearchHistoryDao {

    /**
     * Returns an entity by a food name.
     * @param foodName name of the food
     * @return returns the entity related to
     */
    @Query("SELECT * FROM SEARCH_HISTORY WHERE FOOD_NAME = :foodName")
    SearchHistory findByFoodName(String foodName);

    /**
     * Inserts a new record.
     * @param searchHistory new entity
     */
    @Insert
    void insert(SearchHistory searchHistory);

}
