package com.avanwalre.mybeerhalf;

import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.avanwalre.mybeerhalf.activities.MainActivity;
import com.avanwalre.mybeerhalf.activities.MatchesActivity;
import com.avanwalre.mybeerhalf.utils.RecyclerViewItemCountAssertion;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * ATTENTION! Before running these tests, it is mandatory to keep data connection OFF!
 */
@RunWith(AndroidJUnit4.class)
public class MatchesActivityOfflineEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void writeFoodAndObtainData() {
        final String FOOD_NAME = "fish";
        final int NUMBER_OF_RESULTS = 18;

        Intents.init();

        try {
            onView(withId(R.id.main_field_food)).perform(typeText(FOOD_NAME), closeSoftKeyboard());
            onView(withId(R.id.main_button_search)).perform(click());

            intended(hasComponent(MatchesActivity.class.getName()));

            onView(withId(R.id.matches_rv_beers)).check(new RecyclerViewItemCountAssertion(NUMBER_OF_RESULTS));
        } finally {
            Intents.release();
        }
    }

    @Test
    public void writeFoodAndNotObtainData() {
        final String FOOD_NAME = "patates braves" + new Random().nextInt();

        Intents.init();

        try {
            onView(withId(R.id.main_field_food)).perform(typeText(FOOD_NAME), closeSoftKeyboard());
            onView(withId(R.id.main_button_search)).perform(click());

            intended(hasComponent(MatchesActivity.class.getName()));

            onView(allOf(withId(R.id.matches_noresult), withText(R.string.matches_label_noconnection))).check(matches(isDisplayed()));
        } finally {
            Intents.release();
        }
    }


}