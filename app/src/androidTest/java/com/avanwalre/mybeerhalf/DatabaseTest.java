package com.avanwalre.mybeerhalf;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.avanwalre.mybeerhalf.dao.SearchHistoryDao;
import com.avanwalre.mybeerhalf.database.AppDatabase;
import com.avanwalre.mybeerhalf.entities.SearchHistory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {

    private SearchHistoryDao searchHistoryDao;
    private AppDatabase db;

    private final String FOOD_NAME = "omelette";

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        searchHistoryDao = db.searchHistoryDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void insertAndGetRecordEqual() {
        searchHistoryDao.insert(new SearchHistory().foodName(FOOD_NAME).jsonData("this_is_an_example"));
        SearchHistory searchHistoryResult = searchHistoryDao.findByFoodName(FOOD_NAME);
        assertNotNull(searchHistoryResult);
    }

    @Test
    public void insertAndGetRecordNonEqual() {
        SearchHistory searchHistoryResult = searchHistoryDao.findByFoodName(FOOD_NAME + "_");
        assertNull(searchHistoryResult);
    }

}
