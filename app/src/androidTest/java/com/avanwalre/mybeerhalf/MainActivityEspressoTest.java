package com.avanwalre.mybeerhalf;

import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.avanwalre.mybeerhalf.activities.MainActivity;
import com.avanwalre.mybeerhalf.activities.MatchesActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void writeAndLaunchMatchesActivity() {
        Intents.init();

        onView(withId(R.id.main_field_food))
                .perform(typeText("potato"), closeSoftKeyboard());

        onView(withId(R.id.main_button_search)).perform(click());

        intended(hasComponent(MatchesActivity.class.getName()));

        Intents.release();
    }

    @Test
    public void writeNothingAndStayAtSameActivity() {
        onView(withId(R.id.main_field_food))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.main_button_search)).perform(click());

        hasComponent(MainActivity.class.getName());
    }

}